﻿using System;
using System.Collections.Generic;
using System.Text;
using static Temp.Del;

namespace Temp
{
    public class Del
    {
        public delegate void MyDelegate(string message);
    }
    public class MYClass
    {
        //public delegate void MyDelegate(string message);
        public event MyDelegate MyEvent;
        public MYClass(int count)
        {
            Count = count;
        }
        public int Count { get; private set; }
        public void Sum(int c)
        {
            Count += c;
            MyEvent?.Invoke($"Add {c}");
        }
        public void Sub(int c)
        {
            Count -= c;
            MyEvent?.Invoke($"Sub {c}");
        }
    }
}
