﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Temp
{
    class SettingSingltone
    {
        private static SettingSingltone settings;
        public List<Setting> Settings { get; set; } = new List<Setting>();

        private SettingSingltone()
        {

        }

        public static SettingSingltone GetSettingSingltone()
        {
            if (settings == null)
            {
                settings = new SettingSingltone();
            }

            return settings;
        }
    }

    class Setting
    {
        public string Name { get; set; }
        public object Value { get; set; }
    }
}
