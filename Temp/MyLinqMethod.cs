﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Temp
{
    public static class MyLinqMethod
    {
        public delegate bool MyFunc<T>(T item);
        public delegate int MyFuncGeneric<T>(T item);
        public delegate int MyFuncMax();
        public static IEnumerable<T> MyWhere<T>(this IEnumerable<T> list, 
            MyFunc<T> condition)
        {
            var newList = new List<T>();
            foreach (var item in list)
            {
                if (condition(item))
                {
                    newList.Add(item);
                }
            }
            return newList;
        }

        public static int MyMax(this IEnumerable<int> list)
        {
            var myItem = 0;
            foreach (var item in list)
            {
                if (myItem < item)
                {
                    myItem = item;
                }
            }
            return myItem;
        }

        public static int MyMaxGeneric<T>(this IEnumerable<T> list,
            MyFuncGeneric<T> someGet)
        {
            var listItems = new List<int>();
            foreach (var item in list)
            {
                var result = someGet(item);
                listItems.Add(result);
            }

            var myItem = 0;
            foreach (var item in listItems)
            {
                if (myItem < item)
                {
                    myItem = item;
                }
            }
            return myItem;
        }
    }
}
