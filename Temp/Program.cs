﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;

namespace Temp
{
    class MyCustomException : DivideByZeroException
    {

    }
    class MyClassic
    {
        public static int countStatic;
        public int countNonstatic;
        public MyClassic()
        {
            countStatic++;
        }
    }
    class Program
    {
        private static bool SomeWhere(int i)
        {
            return i > 600;
        }

        static void MyAssemblyLoadEventHandler(object sender, AssemblyLoadEventArgs args)
        {
            Console.WriteLine("ASSEMBLY LOADED: " + args.LoadedAssembly.FullName);
            Console.WriteLine();
        }
        private static void Calc()
        {
            int result = 0;
            var x = 5;
            int y = 0;
            try
            {
                result = x / y;
            }
            catch (MyCustomException e)
            {
                Console.WriteLine("Catch DivideByZeroException");
                throw;
            }
            catch (Exception e)
            {
                Console.WriteLine("Catch Exception");
            }
            finally
            {
                throw new MyCustomException();
            }
        }
        static void Print(object obj)
        {
            Console.WriteLine($"{obj}, managedThreadId:{Thread.CurrentThread.ManagedThreadId}");
        }

        static void Main(string[] args)
        {
            Thread t = Thread.CurrentThread;
            Console.OutputEncoding = Encoding.UTF8;
            //получаем имя потока
            Console.WriteLine($"Имя потока: {t.Name}");
            t.Name = "Метод Main";
            Console.WriteLine($"Имя потока: {t.Name}");

            Console.WriteLine($"Запущен ли поток: {t.IsAlive}");
            Console.WriteLine($"Приоритет потока: {t.Priority}");
            Console.WriteLine($"Статус потока: {t.ThreadState}");
            return;
            for (int i1 = 0; i1 < 3; i1++)
            {
                ThreadPool.QueueUserWorkItem(Print, $"{i1}");
            }
            Thread.Sleep(100);
            return;
            var myClass1 = new MyClassic();
            myClass1.countNonstatic = 5;
            MyClassic.countStatic += 3;
            Console.WriteLine(MyClassic.countStatic);
            var myClass2 = new MyClassic();
            myClass2.countNonstatic = 6;
            Console.WriteLine(MyClassic.countStatic);
            return;
            var connectionString = @"Server=DESKTOP-QP982B5; Initial Catalog='DB_for_SPA_Angular'; Integrated Security=true;";
            using (var connection = new SqlConnection(connectionString))
            {
                connection.Open();
                var command = connection.CreateCommand();
                command.CommandText = "Select WordForLearn from Word";
                Console.OutputEncoding = Encoding.UTF8;
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Console.WriteLine($"Word = {reader["WordForLearn"]}");
                    }
                }
            }
            using (var connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand(
                  "SELECT WordForLearn FROM Word;",
                  connection);
                connection.Open();

                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Console.WriteLine("{0}", reader.GetString(0));
                    }
                }
                else
                {
                    Console.WriteLine("No rows found.");
                }
                reader.Close();
            }
            return;
            var singlton = SettingSingltone.GetSettingSingltone();
            singlton.Settings.Add(new Setting { Name = "My1", Value = 23 });


            var singlton2 = SettingSingltone.GetSettingSingltone();
            var setting = singlton2.Settings.First(x => x.Name == "My1");
            Console.WriteLine(setting.Value);

            try //Catch Exception, Catch MyCustomException
            {
                Calc();
            }
            catch (MyCustomException e)
            {
                Console.WriteLine("Catch MyCustomException");
                throw;
            }
            catch (DivideByZeroException e)
            {
                Console.WriteLine("Catch Exception");
            }
            return;
            var cc = new C();
            A aa = cc;

            aa.Print2();
            aa.Print1();
            cc.Print2();
            return;


            var s1 = string.Format("{0}{1}", "abc", "cba");
            var s2 = "abc" + "cba";
            var s3 = "abccba";

            Console.WriteLine(s1 == s2);
            Console.WriteLine((object)s1 == (object)s2);
            Console.WriteLine(s2 == s3);
            Console.WriteLine((object)s2 == (object)s3);
            return;
            int i2 = 1;
            object obj = i2;
            ++i2;
            Console.WriteLine(i2);
            Console.WriteLine(obj);
            Console.WriteLine(short.Parse(obj.ToString()));
            Console.WriteLine((short)obj);
            return;
            B obj1 = (B)new A();
            //obj1.Foo();

            B obj2 = new B();
            //obj2.Foo();

            A obj3 = new B();
            //obj3.Foo();

            List<Action> actions = new List<Action>();
            for (int count1 = 0; count1 < 10; count1++)
            {
                actions.Add(() => Console.WriteLine(count1));
            }
            foreach (var action in actions)
            {
                action();
            }
            return;
            int ii = 1;
            ii++;
            Console.WriteLine(ii++);
            Console.WriteLine(ii);
            int jj = 1;
            ++jj;
            Console.WriteLine(++jj);
            return;
            AppDomain currentDomain = AppDomain.CurrentDomain;
            currentDomain.AssemblyLoad += new AssemblyLoadEventHandler(MyAssemblyLoadEventHandler); ;

            IEnumerable<int> some = new List<int> { 23, 34, 45, 56, 57, 70, 678 };
            some = some.MyWhere(x => x > 50)
                        .MyWhere(x => x < 1000).ToList();
            some = some.MyWhere(delegate (int i)
            {
                return i > 60;
            });
            var delegate1 = new MyLinqMethod.MyFunc<int>(SomeWhere);
            some = some.MyWhere(delegate1);
            foreach (var item in some)
            {
                Console.WriteLine(item);
            }

            var intList = new List<int> { 23, 34, 2, 32, 65, 22 };
            var result2 = intList.MyMax();
            var cats = new List<Cat> {
                new Cat { age = 34, name = "sdf" },
                new Cat { age = 31, name = "sdf2" },
                new Cat { age = 39, name = "sdf3" },
            };
            var result3 = cats.MyMaxGeneric(x => x.age);
            Console.WriteLine($"Max cat's age '{result3}'");
            return;

            //var some = new MulticastDelegate
            var cat1 = new Cat { age = 13, name = "Lusa" };
            cat1.ShowCat1();
            cat1.ShowCat2();
            Test a = new Test();
            Test b = new Test();
            a.x = 1;
            Console.WriteLine(a.x); // 3
            b.x = 2;
            Console.WriteLine(a.x); // 3
            b = a; // присвоение ссылки
            b.x = 3;
            Console.WriteLine(a.x); // 3
            Console.WriteLine(b.x); // 3
            a.x = 1;
            Console.WriteLine(a.x); // 3
            Console.WriteLine(b.x); // 3
            Console.WriteLine("----------");
            int count = 5;
            Console.WriteLine(count);
            Counter(ref count);
            Console.WriteLine(count);
            Console.WriteLine("----------");
            int i, j; // Variable need not be initialized 
            sampleMethod(out i, out j);
            Console.WriteLine(i);
            Console.WriteLine(j);
            Console.WriteLine("----------");
            (int, string) tuple1 = (1, "a");
            (int index, string name) tuple2 = (2, "b");
            var temp = tuple1.Item1 + tuple2.index;
            Console.WriteLine(temp);
            Console.WriteLine("----------");
            //try
            //{
            //    int k = 5;
            //    int d = k / 0;
            //}
            //catch (Exception ex)
            //{
            //    //throw new PersonException(ex.Message);
            //    throw new Exception("sdf", ex);
            //    //throw ex;
            //}
            //finally
            //{
            //    Console.WriteLine("It`s bad idea");
            //}
            IntOperation op1;
            op1 = Sum;
            int result = op1(5, 10);
            Console.WriteLine("Sum: " + result);

            // Изменим ссылку на метод
            op1 = new IntOperation(Prz);
            result = op1(5, 10);
            Console.WriteLine("Mul: " + result);
            Console.WriteLine("------------delegate");
            var weather = new Weather();
            var deleg = new Message(weather.Sunny);
            deleg();
            deleg += weather.Cloudy;
            deleg();

            Summa del1 = SomeVar();

            for (int k = 1; k <= 5; k++)
            {
                Console.WriteLine("sum {0} is: {1}", k, del1(k));
            }
            Console.WriteLine("---------------------events");
            var myClass = new MYClass(10);
            myClass.MyEvent += Show;
            myClass.Sum(2);
            myClass.Sub(3);
        }
        static void Show(string mess)
        {
            Console.WriteLine(mess);
        }
        static Summa SomeVar()
        {
            int result = 0;


            Summa del = delegate (int number)
            {
                for (int i = 0; i <= number; i++)
                    result += i;
                return result;
            };
            return del;
        }
        delegate int IntOperation(int i, int j);
        static int Sum(int x, int y)
        {
            return x + y;
        }

        static int Prz(int x, int y)
        {
            return x * y;
        }

        static int Del(int x, int y)
        {
            return x / y;
        }
        public static void Counter(ref int c)
        {
            c++;
        }
        public static void sampleMethod(out int sampleData1, out int sampleData2)
        {
            sampleData1 = 10;
            sampleData2 = 20;
            //return 0;
        }
        delegate void Message();
        delegate int Summa(int number);
    }

    public class Weather
    {
        public void Sunny()
        {
            Console.WriteLine("Today sunny");
        }
        public void Cloudy()
        {
            Console.WriteLine("Today cloudy");
        }
    }

    class PersonException : Exception
    {
        public PersonException(string message)
            : base(message)
        { }
    }
    class MyClass
    {
        int[] arr;
        public IEnumerator<int> GetEnumerator()
        {
            foreach (var item in arr)
            {
                yield return item;
            }
        }
    }

    class Test
    {
        public int x;
    }
    public class Cat
    {
        internal string name;
        public int age;
        public Cat()
        {

        }
        public void ShowCat1()
        {
            Console.WriteLine($"from public - {name}, {age}");
        }
        internal void ShowCat2()
        {
            Console.WriteLine($"from private - {name}, {age}");
        }
    }
    class Dog : Cat
    {
        public int weigth;
        public Dog() : base()
        {

        }
        internal void ShowWeigth()
        {
            Console.WriteLine($"from private - {weigth}");
        }
    }
}
